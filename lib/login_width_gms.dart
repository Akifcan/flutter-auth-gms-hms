import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/button_view.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginWithGms extends StatefulWidget {
  const LoginWithGms({Key? key}) : super(key: key);

  @override
  _LoginWithGmsState createState() => _LoginWithGmsState();
}

class _LoginWithGmsState extends State<LoginWithGms> {


   String loginId = '';
   String displayName = '';
   String avatar = '';


  void signIn() async {
      final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();
      final GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;
      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth?.accessToken,
        idToken: googleAuth?.idToken,
      );
      UserCredential user = await FirebaseAuth.instance.signInWithCredential(credential);
      setState(() {
        loginId = user.user!.uid;
        displayName = user.user!.displayName.toString();
        avatar = user.user!.photoURL.toString();
      });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

            Text(loginId),
            Text(avatar),
            Text(displayName),

            SignInButton(
              Buttons.Google,
              onPressed: signIn,
            )
          ],
        ),
      ),
    );
  }
}