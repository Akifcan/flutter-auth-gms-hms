import 'package:flutter/material.dart';
import 'package:huawei_account/huawei_account.dart';


class LoginWithHms extends StatefulWidget {
  const LoginWithHms({Key? key}) : super(key: key);

  @override
  _LoginWithHmsState createState() => _LoginWithHmsState();
}

class _LoginWithHmsState extends State<LoginWithHms> {

    String loginId = '';
    String displayName = '';
    String avatar = '';

    void login() async {
      AccountAuthParamsHelper helper = AccountAuthParamsHelper();
      helper.setEmail();
      helper.setProfile();
      helper.setCarrierId();
      helper.setEmail();
      helper.setIdToken();
      helper.setDialogAuth();
      AuthAccount account =  await AccountAuthService.signIn(helper);
      setState(() {
        loginId = account.unionId.toString();
        avatar = account.avatarUri!.isNotEmpty ? account.avatarUri.toString() : "set default";
        displayName = account.displayName.toString();
      });
      print("email token: " + account.email.toString());
      print("avatar token: " + account.avatarUri.toString());
      print("display token: " + account.displayName.toString());
      print("given token: " + account.givenName.toString());
      print("gender token: " + account.gender.toString());
      print("gender token: " + account.unionId.toString());
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(loginId),
            Text(avatar),
            Text(displayName),
            HuaweiIdAuthButton(
              theme: AuthButtonTheme.FULL_TITLE,
              buttonColor: AuthButtonBackground.RED,
              borderRadius: AuthButtonRadius.MEDIUM,
              onPressed: login,
            ),
          ],
        )
      ),
    );
  }
}