import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:device_info/device_info.dart';
import 'package:iepls/login_width_gms.dart';
import 'package:iepls/login_width_hms.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String device = '';
  bool loaded = false;


  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        if(device != "HUAWEI"){
          await Firebase.initializeApp();

        }
        setState(() {
          device = androidInfo.manufacturer;
        });
     });
  }


  @override
  Widget build(BuildContext context) {
    if(device.isEmpty){
      return const Scaffold(
        body: Text("wait"),
      );
    }
    if(device == "HUAWEI"){
      return const LoginWithHms();
    }
    return LoginWithGms();
  }
}